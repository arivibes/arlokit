//
//  Routes.swift
//  ArloKit
//
//  Created by Ariel Elkin on 25/05/2017.
//
//

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer

import SwiftString

func indexHandler(request: HTTPRequest, _ response: HTTPResponse) {

    if let host = request.header(.host) {

        if request.path.containsSubstring(input: "arify_owncloud") {
            response.status = .temporaryRedirect
            response.setHeader(.location, value: "http://\(host):7070")
        }

        else if request.path.containsSubstring(input: "arify") {
            response.status = .temporaryRedirect
            response.setHeader(.location, value: "http://\(host):4040")
        }

        else if request.path.containsSubstring(input: "seedbox") {
            response.status = .temporaryRedirect
            response.setHeader(.location, value: "http://\(host):9091")
        }

        else if request.path.containsSubstring(input: "backup") {
            response.status = .temporaryRedirect
            response.setHeader(.location, value: "http://\(host):8084")
        }

        else if request.path.containsSubstring(input: "realtime-audio-on-ios-tutorial-making-a-mandolin") {
            response.status = .movedPermanently
            response.setHeader(.location, value: "https://arielelkin.github.io/articles/mandolin")
        }

        else {
            response.status = .movedPermanently
            response.setHeader(.location, value: "https://arielelkin.github.io/")
        }



    }
    else {
        Log.error(message: "Could not handle request with path: \(request.path)")
    }

    response.completed()
}


extension String {
    func containsSubstring(input: String) -> Bool {
        return self.index(of: input) != -1
    }
}
