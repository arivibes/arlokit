//
//  main.swift
//  ArloKit
//
//  Created by Ariel Elkin on 25/05/2017.
//
//

import PerfectLib
import PerfectHTTP
import PerfectHTTPServer
import PerfectRequestLogger

let server = HTTPServer()
server.serverPort = 80

var routesEighty = Routes()
routesEighty.add(method: .get, uris: ["/*"], handler: indexHandler)
server.addRoutes(routesEighty)


// Instantiate a logger
let myLogger = RequestLogger()


// Add the filters
// Request filter at high priority to be executed first
server.setRequestFilters([(myLogger, .high)])
// Response filter at low priority to be executed last
server.setResponseFilters([(myLogger, .low)])



do {
    try server.start()
}
catch PerfectError.networkError(let err, let msg) {
    Log.critical(message: "Network error thrown: \(err) \(msg)")
}
