# docker image build -t arlokit .
# docker run --privileged -ti -p 80:80 -d --name arlokit arlokit .build/debug/ArloKit

FROM swift:3.1

RUN apt-get -y update && apt-get install -y \
    openssl libssl-dev uuid-dev

WORKDIR /arlokit
ADD Package.swift Package.swift
ADD Sources/ Sources/
RUN swift build

EXPOSE 80
